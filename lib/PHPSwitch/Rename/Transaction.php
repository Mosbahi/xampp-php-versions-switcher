<?php
class PHPSwitch_Rename_Transaction
{
    /**
     * List of PHPSwitch_Rename instances
     * @var  array
     */
    private $_instances = array();
   
    /**
     * Flag to store the transaction status
     * @var  bool
     */
    private $_failure = false;


    /**
     * Constructor
     */
    public function __construct()
    {
        // donut
    }


    /**
     * Adds a PHPSwitch_Rename to the list
     *
     * @param  PHPSwitch_Rename  $obj
     */
    public function add(PHPSwitch_Rename $obj)
    {
        $this->_instances[] = $obj;
    }


    /**
     * Runs the transaction. If one of of the existing steps return a failure, further 
     * processing will be skipped.
     *
     * @return  bool  True in success otherwhise false
     */
    public function run()
    {
        foreach ($this->_instances as $obj) {
            if (!$res = $obj->execute()) {
                $this->_failure = true;
                return false;
            }
        }
        return true;
    }


    /**
     * Runs the rollback of previous executed commands. The rollback should reset all previous done 
     * changes. If the rollback for whatever reason also fails, it's manual time ;-)
     *
     * @return  bool  True in success otherwhise false
     */
    public function rollback()
    {
        if (!$this->_failure) {
            return true;
        }

        foreach ($this->_instances as $obj) {
            if (!$res = $obj->undo()) {
                $this->_failure = true;
                return false;
            }
        }
        $this->_failure = false;
        return true;
    }
}


