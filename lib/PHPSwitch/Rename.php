<?php
class PHPSwitch_Rename
{
    /**
     * Old file/folder name
     * @var  string
     */
    private $_oldName;

    /**
     * New file/folder name
     * @var  string
     */
    private $_newName;

    /**
     * Flag to store successfull renaming status
     * @var  bool
     */
    private $_renamed = false;


    /**
     * Constructor
     *
     * @param  string  $oldName  Old file/folder name
     * @param  string  $newName  New file/folder name
     */
    public function __construct($oldName, $newName)
    {
        $this->_oldName = $oldName;
        $this->_newName = $newName;
    }

    /**
     * The main function, which does the rename job
     *
     * @return  bool  True on success otherwhise false
     */
    public function execute()
    {
        PHPSwitch::_d("Old name: $this->_oldName" . LF . "New name: $this->_newName");
        if ($res = rename($this->_oldName, $this->_newName)) {
            $this->_renamed = true;
        }
        return $res;
    }

    /**
     * Function to undo a previous successfull rename command
     *
     * @return  bool  True on success otherwhise false
     */
    public function undo()
    {
        if ($this->_renamed == false) {
            return true;
        }
        if ($res = rename($this->_newName, $this->_oldName)) {
            $this->_renamed = false;
        }
        return $res;
    }
}
