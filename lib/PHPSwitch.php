<?php
class PHPSwitch
{
    /**
     * Self instance
     * @var  PHPSwitch
     */
    private static $_instance = null;

    /**
     * Debug status
     * @var  bool
     */
    private static $_debug = true;

    /**
     * Configuration array
     * @var  array
     */
    private $_cfg = array();

    /**
     * List of found installations
     * @var  array
     */
    private $_installations = array();

    /**
     * Internal counter, will be incremented on each installation, where the version is not detectable
     * @var  int
     */
    private $_unknownCounter = 0;


    /**
     * Constructor (prevent public instantiation)
     */
    protected function __construct()
    {
        // donut
    }


    /**
     * Prevent cloning
     */
    private function __clone()
    {
        // donut
    }


    /**
     * Returns self instance which will be creatd once.
     *
     * @return  PHPSwitch
     */
    public static function getInstance()
    {
        if (self::$_instance == null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    /**
     * Checks, if apache is running.
     *
     * @return  bool
     */
    public function isApacheRunning()
    {
        $isRunning = false;
        ini_set('default_socket_timeout', '3');
        if (false !== ($handle = @fopen('http://127.0.0.1/', 'r'))) {
            fclose($handle);
            $isRunning = true;
        }
        unset($handle);
        return $isRunning;
    }


    /**
     * Initializes the PHP switch, read the configuration file (ini file) and an found PHP 
     * installations.
     *
     * @return  int|bool  Either true on success or a digit less than 0 as a error status as follows:
     *                    - -1 = Invalid PHP installation path
     *                    - -2 = No PHP installation found
     *                    - -3 = The exists only one installation, nothing to switch
     */
    public function initialize()
    {
        // some settings
        $cfg = parse_ini_file(PHPSWITCH_DIR . 'config.ini');
        $this->_cfg = array_merge($this->_cfg, $cfg);

        if (!isset($this->_cfg['phpInstallationsPath']) || !is_dir($this->_cfg['phpInstallationsPath'])) {
            return -1;
        }

        if (!isset($this->_cfg['phpIgnoreDirs']) && $this->_cfg['phpIgnoreDirs'] == '') {
            $this->_cfg['phpIgnoreDirs'] = array();
        } else {
            $this->_cfg['phpIgnoreDirs'] = explode(',', $this->_cfg['phpIgnoreDirs']);
        }

        if (isset($this->_cfg['debugEnable'])) {
            self::$_debug = ($this->_cfg['debugEnable'] == '1') ? true : false;
        }

        if (!isset($this->_cfg['phpDirName']) || $this->_cfg['phpDirName'] == '') {
            $this->_cfg['phpDirName'] = 'php';
        }

        // set all installations
        $this->_setInstallations();

        if (count($this->_installations) == 0) {
            return -2;
        }

        $installations = $this->getInstallationsExceptCurrent();
        if (count($installations) == 0) {
            return -3;
        }

        return true;
    }


    /**
     * Returns the current active installation
     *
     * @return  array|null  The current installation array or null, if nothing exists.
     */
    public function getCurrentInstallation()
    {
        foreach ($this->_installations as $pos => $item) {
            if ($item['current']) {
                return $item;
            }
        }
        return null;
    }


    /**
     * Returns all found PHP installations except the current active one
     *
     * @return  array  The php installations array
     */
    public function getInstallationsExceptCurrent()
    {
        $installations = array();
        foreach ($this->_installations as $pos => $item) {
            if ($item['current']) {
                continue;
            }
            $installations[] = $item;
        }
        return $installations;
    }


    /**
     * Handles the userinteraction by using stdin and returns back the user selection
     *
     * @return  string  Either 'x' to exit or a digit ('1', '2', '3', etc.), representing the 
     *                  selected option position.
     */
    public function getUserSelection()
    {
        $currInstallation = $this->getCurrentInstallation();
        $installations    = $this->getInstallationsExceptCurrent();

        $userInput = '0';

        set_time_limit(0);
        $hStdin = fopen('php://stdin', 'r');
        while ($userInput == '0') {

            if ($currInstallation) {
                PHPSwitch::_('Current running PHP version is ' . $currInstallation['version']);
            }
            PHPSwitch::_('Type number or "x" (exit) for selecting your choice!' );

            $options = '';
            foreach ($installations as $pos => $item) {
                $p = $pos + 1;
                $v = $item['version'];
                $options .= $p . ') Switch to PHP ' . $v . LF;
            }
            $options .= 'x) Exit';
            PHPSwitch::_($options);

            $userInput = (trim(fgets($hStdin, 256)));
            sleep(1);
            if ($userInput == 'x') {
                break;
            } else {
                $pos = ((int) $userInput);
                if ($pos <= 0 || $pos > count($installations)) {
                    PHPSwitch::_('Invalid selection. Type number or "x" (exit) for selecting your choice!' . LF
                        . 'Invalid selection. Enter the number or "x" (exit) to choose!'
                    );
                } else {
                    break;
                }
            }
        }
        fclose($hStdin);

        return $userInput;
    }


    /**
     * Does the PHP switch. 
     *
     * @param   string  The selected position from the displayed options list.
     * @return  bool  True on success otherwhise false.
     */
    public function doTheSwitchPHP($version)
    {
        $currInst = $this->getCurrentInstallation();
        $installations    = $this->getInstallationsExceptCurrent();
        $pos = (int) $version;
        if ($pos <= 0 || $pos > count($installations)) {
            return - 1;
        }
        $pos = $pos-1;

        $newInst = $installations[$pos];
        PHPSwitch::_('Starting switch to PHP ' . $newInst['version'] . LF
            . 'Start with the transition to PHP ' . $newInst['version']
        );

        $renameCur = new PHPSwitch_Rename($currInst['path'], $this->_cfg['phpInstallationsPath'] . $this->_cfg['phpDirName'] . '_' . $currInst['version']);
        $renameNew = new PHPSwitch_Rename($newInst['path'], $this->_cfg['phpInstallationsPath'] . $this->_cfg['phpDirName']);

        $transaction = new PHPSwitch_Rename_Transaction();
        $transaction->add($renameCur);
        $transaction->add($renameNew);

        if (!$transaction->run()) {
            $res = $transaction->rollback();
            return false;
        } else {
            return true;
        }
    }



    public function doTheSwitchXampp($version)
    {
        $currInst = $this->getCurrentInstallation();
        $installations    = $this->getInstallationsExceptCurrent();
        $pos = (int) $version;
        if ($pos <= 0 || $pos > count($installations)) {
            return - 1;
        }
        $pos = $pos-1;

        $newInst = $installations[$pos];
        $o = reset(explode('.',$currInst['version']));
        $n = reset(explode('.',$newInst['version']));


        if($n != $o){
            @unlink($this->_cfg['apacheConfPath'].'httpd-xampp.conf');
            copy($this->_cfg['apacheConfPath'].'httpd-xampp'.$n.'.conf', $this->_cfg['apacheConfPath'].'httpd-xampp.conf' );

        }
        return true;
    }


    /**
     * Static function to print messages.
     *
     * @param  string  $msg
     */
    public static function _($msg)
    {

        $tmp = explode(LF, $msg);
        foreach($tmp as $p => $v) {
            $tmp[$p] = '  ' . $v;
        }
        $msg = implode(LF, $tmp);
        echo LF . $msg . LFD;
    }


    /**
     * Static function to print debug messages.
     *
     * The output will happen, if debugging is enabled (@see PHPSwitch::$_debug)
     *
     * @param  string  $msg
     */
    public static function _d($msg)
    {
        if (!self::$_debug) {
            return;
        }
        $tmp = explode(LF, $msg);
        foreach($tmp as $p => $v) {
            $tmp[$p] = '  ## ' . $v;
        }
        $msg = implode(LF, $tmp);
        echo LF . $msg . LFD;
    }


    /**
     * Reads the PHP installations path and collects all found PHP installations.
     */
    private function _setInstallations()
    {
        // open php installations dir
        if (!$handle = opendir($this->_cfg['phpInstallationsPath'])) {
            return;
        }

        // read all matching php directories
        $debug = '';
        while ($file = readdir($handle)) {
            if ($file == '.' || $file == '..' || !is_dir($this->_cfg['phpInstallationsPath'] . $file)) {
                continue;
            }
            if (strpos($file, $this->_cfg['phpDirName']) === 0 && !in_array($file, $this->_cfg['phpIgnoreDirs']) && 
                is_file($this->_cfg['phpInstallationsPath'] . $file . '/php.exe')) {
                $debug .= 'Found PHP installation: ' . $this->_cfg['phpInstallationsPath'] . $file . LF;
                $this->_installations[] = array(
                    'path'    => $this->_cfg['phpInstallationsPath'] . $file,
                    'current' => ($file == $this->_cfg['phpDirName']),
                    'version' => PHPSwitch::_readPHPVersion($this->_cfg['phpInstallationsPath'] . $file . '/PHP_VERSION')
                );
            }
        }
        self::_d($debug);
        
        closedir($handle);
    }


    /**
     * Reads the PHP version from a found PHP installation.
     * 
     * @param   string  $filePathName  The path and name of the file containing the PHP version
     * @return  string  The version
     */
    private function _readPHPVersion($filePathName)
    {
        $version = '';
        if (is_file($filePathName)) {
            $version = trim(file_get_contents($filePathName));
        }
        if ($version == '') {
            $version = $this->_getNextUnknownPos();
        }
        return $version;
    }


    /**
     * Returns the next unknown position string which will be used to mark unknown PHP versions.
     * 
     * @param  string  The unknown position string
     */
    private function _getNextUnknownPos()
    {
        return 'Unknown PHP version ' . (string) ++$this->_unknownCounter;
    }

}
