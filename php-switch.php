<?php

// some constants
define('LF', "\r\n");
define('LFD', "\r\n\r\n");
define('PHPSWITCH_DIR', str_replace('\\', '/', realpath(dirname(__FILE__) . '/')) . '/');

require_once(PHPSWITCH_DIR . 'lib/PHPSwitch.php');
require_once(PHPSWITCH_DIR . 'lib/PHPSwitch/Rename.php');
require_once(PHPSWITCH_DIR . 'lib/PHPSwitch/Rename/Transaction.php');

$switch = PHPSwitch::getInstance();


// apache check
if ($switch->isApacheRunning()) {
    PHPSwitch::_('The Apache is running! Please stop the Apache before make this procedure!' . LF
        . 'PHP Switch exit ...'
    );
    exit;
}


// initialize php switch
$res = $switch->initialize();
if ($res === -1) {
    PHPSwitch::_('Missing or invalid configuration "phpInstallationsPath"!' . LF
        . 'PHP Switch exit ...'
    );
    exit;
} elseif ($res === -2) {
    PHPSwitch::_('Could not find any installed PHP Versions, ' . LF
        . '  please check your configuration "phpInstallationsPath"!' . LF
        . 'PHP Switch exit ...'
    );
    exit;
} elseif ($res === -3) {
    $curr = $switch->getCurrentInstallation();
    PHPSwitch::_('Could not find further installed PHP Versions ' . LF
        . '  other than current running one ' . $curr['version'] . '!' . LF
        . 'PHP Switch exit ...'
    );
    exit;
}


// get user selection
$selection = $switch->getUserSelection();
if ($selection == 'x') {
    PHPSwitch::_('PHP Switch is terminating on demand ...' . LF
        . 'PHP Switch exit ...'
    );
    sleep(1);
    exit;
}


// handle the switch
$res = $switch->doTheSwitchPHP($selection);
if ($res === -1) {
    PHPSwitch::_('Invalid switch selection ' . $selection . LF
        . 'PHP Switch exit ...'
    );
    exit;
}

// handle the switch
$res = $switch->doTheSwitchXampp($selection);
if ($res === -1) {
    PHPSwitch::_('Invalid switch selection ' . $selection . LF
        . 'PHP Switch exit ...'
    );
    exit;
}

return;
